import requests
from bs4 import BeautifulSoup

codes = ['096530', '010130'] # 종목코드 리스트
prices = [] # 가격정보가 담길 리스트

for code in codes:
    url = 'https://finance.naver.com/item/main.nhn?code=' + code

    response = requests.get(url)
    response.raise_for_status()
    html = response.text # 일반 텍스트 문서
    soup = BeautifulSoup(html, 'html.parser') # 원하는 html 태그 값을 추출할 수 있음

    today = soup.select_one('#chart_area > div.rate_info > div')
    print(today)
    price = today.select_one('.blind')
    print(price)
    prices.append(price.get_text())

print(prices)