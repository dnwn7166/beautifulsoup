from selenium import webdriver

naverNewsUrl = 'https://news.naver.com/main/main.naver?mode=LSD&mid=shm&sid1=100'
driver = webdriver.Chrome(r'C:\Users\chromedriver.exe')

tmNews = driver.find_element_by_id('cluster_text')
tmNewsLis = tmNews.find_element_by_tag_name('a')

for li in tmNewsLis:
    aTag = li.find_element_by_tag_name('a')
    href = aTag.get_attribute('href')

    print('기사 제목 : ', aTag.text)
    print('링크 : ', href)
