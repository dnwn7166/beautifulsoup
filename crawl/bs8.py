import requests
from bs4 import BeautifulSoup
import openpyxl

HEADERS = ({
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Accept-Language': 'en-US, en;q=0.5'
})



wb = openpyxl.Workbook()
sheet = wb.active
sheet.append(["name", "title", "cont", "color", "rating", "date", "page"])

count = 1



url = "https://www.amazon.com/Samsung-Bundle-Cordless-Stick-Vacuum/product-reviews/" \
      "B091SPSRLS/ref=cm_cr_arp_d_paging_btm_next_2?ie=UTF8&reviewerType=all_reviews&pageNumber=" + str(count)

response = requests.get(url, headers=HEADERS)
result = response.text.find("From the United States")
soup = BeautifulSoup(response.text, "html.parser")

comments = soup.select("#cm_cr-review_list > div")
for comment in comments:
    status = comment.select("div:nth-child(1) > a > div.a-profile-content > span")
    if status:
        print("=====================================")
        print(comment.select("div:nth-child(1) > a > div.a-profile-content > span")[0].text)
        print(comment.select("div:nth-child(2) > a.a-size-base.a-link-normal.review-title.a-color-base.review-title-content.a-text-bold > span")[0].text)

