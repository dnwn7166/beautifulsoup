import requests
from bs4 import BeautifulSoup

HEADERS = ({
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Accept-Language': 'en-US, en;q=0.5'
})

count = 28

while 1:
    url = "https://www.amazon.com/LG-Cordless-Upholstery-Powerful-Lightweight/" \
          "product-reviews/B07QDVLXNN/ref=cm_cr_arp_d_paging_btm_next_2?ie=UTF8&reviewer" \
          "Type=all_reviews&pageNumber=" + str(count)

    response = requests.get(url, headers=HEADERS)
    result = response.text.find("From the United States")

    if result == -1:
        break
    else:
        count = count + 1




